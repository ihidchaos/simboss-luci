# SIMBOSS OpenWrt luci feed


## Description

This is the SIMBOSS OpenWrt "luci"-feed containing LuCI - OpenWrt Configuration Interface.

## Usage

This feed is enabled by default. Your feeds.conf.default (or feeds.conf) should contain a line like:
```
src-git luci https://gitee.com/skyaplus/luci-simboss.git
```

To install all its package definitions, run:
```
./scripts/feeds update luci
./scripts/feeds install -a -p luci
```

simboss luci based on Opensource Openwrt luci engine.